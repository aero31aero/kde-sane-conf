#! /usr/bin/env bash

root="/home/rohitt/data-drive/rohitt" # put your home directory here.
target="/home/rohitt/Documents/projects/kde-conf/manjaro-backup" # specify where you want the backup
# root="/home/rohitt"
# target="/home/rohitt/Documents/projects/kde-conf/arch-backup"

echo "$target"
mkdir -p "$target"
mkdir -p "$target/config"
cat "/home/rohitt/Documents/projects/kde-conf/kde-sane-config/mappings.json" | jq -r '.[]|[.from, .to, .comment, .enabled] | @tsv' |
  while IFS=$'\t' read -r from to comment enabled; do
    if [ "$enabled" = "true" ]; then
      if [ "$comment" = "(placeholder)" ]; then
        comment=""
      fi
      echo "Moving: $root/$from -> $target/$to ($comment)"
      [[ -e "$root/$from" ]] && cp -r "$root/$from" "$target/$to"
    fi
  done
