# kde-sane-conf

This is something I wrote for myself to backup/restore my kde config
between distros and machines.

DO NOT RUN THIS DIRECTLY!

You'd need to tweak the variables at the top of the backup/restore scripts
for your setup. Also, install `jq`.

On Arch:

`sudo pacman -S jq`

Then clone this repo and tweak the variables in the backup and restore scripts.

The meat of the project is the list of files anyway, which is in mappings.json.
You could manually use that list instead of running my script directly if needed.
