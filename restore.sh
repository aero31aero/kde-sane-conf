#! /usr/bin/env bash

root="/home/rohitt" # this is the home directory to restore the config to
target="/home/rohitt/Documents/projects/kde-conf/manjaro-backup" # this is where you backup up using the backup script.
# root="/home/rohitt"
# target="/home/rohitt/Documents/projects/kde-conf/arch-backup"

echo "$target"
mkdir -p "$target"
mkdir -p "$target/config"
cat "/home/rohitt/Documents/projects/kde-conf/kde-sane-config/mappings.json" | jq -r '.[]|[.from, .to, .comment, .enabled] | @tsv' |
  while IFS=$'\t' read -r from to comment enabled; do
    if [ "$enabled" = "true" ]; then
      if [ "$comment" = "(placeholder)" ]; then
        comment=""
      fi
      echo "Moving: $root/$from -> $target/$to ($comment)"
      rm -rf "$root/$from"
      [[ -e "$target/$to" ]] && cp -r "$target/$to" "$root/$from"
    fi
  done
